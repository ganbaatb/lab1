const int LEYE = A3;
const int REYE = A4;

const int WheelEncoder = 2;
volatile int count = 0;

const int US_TRIG = 9;
const int US_ECHO = 8;

int motorPinL = 5;
int motorPinR = 6;
int motorPinL2 = 4;
int motorPinR2 = 7;

int distance;
long duration;

void Turning(int a, int b);
void Stopping( );
void ir_isr();


  void setup() {
    Serial.begin(9600);
    pinMode (LEYE,INPUT);
    pinMode (REYE,INPUT);
  
    pinMode (US_TRIG, OUTPUT); 
    pinMode (US_ECHO, INPUT);
  
    pinMode(motorPinL, OUTPUT);
    pinMode(motorPinR, OUTPUT);
    pinMode(motorPinL2, OUTPUT);
    pinMode(motorPinR2, OUTPUT);

    attachInterrupt(digitalPinToInterrupt(WheelEncoder), ir_isr, RISING);
    pinMode(WheelEncoder, INPUT);
  }

  void loop() {

    delay(1000);
    Serial.print("IR counter: ");
    Serial.print(count);

    bool BlackR = digitalRead(LEYE ); 
    bool BlackL = digitalRead(REYE);

    if (BlackL && BlackR ) {
      Turning(150, 150, 0);
      Stopping( );
    }

    else if(!BlackL && !BlackR){
      Turning(150, 150, 0);
      Stopping( );
    }
    
    else if(!BlackL && BlackR){
      Turning(30, 250, 0);
      Stopping( );
      
  }

    else if(BlackL && !BlackR){
      Turning(250, 30, 0);
      Stopping( );
    
    } 

}

  
  void Turning(int a, int b, int c){
      analogWrite(motorPinL, a);
      analogWrite(motorPinR, b);
      analogWrite(motorPinL2, c);
      digitalWrite(motorPinR2 ,LOW); 
  }

  void Stopping( ){
      digitalWrite (US_TRIG, LOW);
      delayMicroseconds(2);
  
      digitalWrite( US_TRIG, HIGH); //not same as notes but i think correct
      delayMicroseconds (10);
      digitalWrite (US_TRIG ,LOW);
  
      duration = pulseIn(US_ECHO, HIGH);
      distance = duration/58;
   
      if(distance <= 15){
        analogWrite(motorPinL,  LOW);
        analogWrite(motorPinR, LOW);
        digitalWrite(motorPinL2, LOW);
        digitalWrite(motorPinR2, LOW);
        delay(1000);
    }
  }
  void ir_isr(){
  count = count+1;
  /**
  revs = count / 5;
  const int diam = 0.062;
  circumf = pie*diam
  distance = revs * circumf
  **/
  
  
}
