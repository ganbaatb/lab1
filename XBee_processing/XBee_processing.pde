import processing.serial.* ;
Serial myPort ;
//myPort = new Serial(this, "COM4", 9600);
String inString; 



void setup()
{
  printArray(Serial.list());
  String portName = Serial.list()[1]; //change to match your port
  myPort = new Serial(this, portName, 9600);
  myPort.write("+++");
  delay(1100);
  myPort.write("ATID 3204, CH C, CN");
  delay(1100);
  myPort.bufferUntil(10);
}

void draw() {
  
}

void keyPressed() {
  myPort.write( key );
  if (keyCode == 10)
    myPort.write('\n');
}
void serialEvent(Serial p) {
  inString = p.readString();
  println(inString);
}
