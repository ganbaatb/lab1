const int LEYE = A3;
const int REYE = A4;
int motorPinL = 4;
int motorPinR = 6;
int motorPinL2 = 5;
int motorPinR2 = 7;


void setup() {

  Serial.begin(9600);
  pinMode (LEYE,INPUT);
  pinMode (REYE,INPUT);

  pinMode(motorPinL, OUTPUT);

  pinMode(motorPinR, OUTPUT);

  pinMode(motorPinL2, OUTPUT);

  pinMode(motorPinR2, OUTPUT);

 
}

void loop() {

   
  bool WhiteL = digitalRead(LEYE ); //all clear is when the buggy sees white
  bool WhiteR = digitalRead(REYE);
  
   if (WhiteL && WhiteR ) {
  analogWrite(motorPinL, 128);
  analogWrite(motorPinR, 250);
  digitalWrite(motorPinL2, LOW);
  digitalWrite(motorPinR2 ,LOW);
 
   }

 
    else if(!WhiteL && !WhiteR){
 digitalWrite(motorPinL, LOW);
 digitalWrite(motorPinR, LOW);
 digitalWrite(motorPinL2, LOW);
 digitalWrite(motorPinR2 ,LOW);
 
 }
 
  //Left side sees black
  // we want left motor to go and right to stop
   else if(!WhiteL){
  analogWrite(motorPinL, 130);
  analogWrite(motorPinR, 30);
  digitalWrite(motorPinL2, LOW);
  digitalWrite(motorPinR2, LOW);
  delay(10);
  }
  
  //Right side sees black
  // we want right motor to go and left to stop
   else if(!WhiteR){
  analogWrite(motorPinL, 30);
  analogWrite(motorPinR, 130);
  digitalWrite(motorPinL2, LOW);
  digitalWrite(motorPinR2, LOW);
  delay(10);
 
  } 
  
}


