const int LED_PIN = 13;
void setup() {

  Serial.begin(9600);
  pinMode (LED_PIN, OUTPUT);

 
}
//turn the LED on and off every second
void loop() {
  digitalWrite( LED_PIN, HIGH);
  delay(1000);
  digitalWrite( LED_PIN, LOW);
  delay(1000);
}
