const int LEYE = A3;
const int REYE = A4;
int motorPinL = 4;
int motorPinR = 6;
int motorPinL2 = 5;
int motorPinR2 = 7;


void setup() {

  Serial.begin(9600);
  pinMode (LEYE,INPUT);
  pinMode (REYE,INPUT);

  pinMode(motorPinL, OUTPUT);

  pinMode(motorPinR, OUTPUT);

  pinMode(motorPinL2, OUTPUT);

  pinMode(motorPinR2, OUTPUT);

 
}

void loop() {
  bool all_clear = digitalRead(LEYE ); //all clear is when the buggy sees white
  bool all_clears = digitalRead(REYE);
  
  if (all_clear && all_clears ) {
    
    digitalWrite(motorPinL, HIGH);
    
    digitalWrite(motorPinR, HIGH);

    digitalWrite(motorPinR2 ,LOW);

    digitalWrite(motorPinL2, LOW);


  }

  else if(!all_clear && !all_clears){

 digitalWrite(motorPinL, LOW);
 
 digitalWrite(motorPinR, LOW);

 digitalWrite(motorPinR2 ,LOW);

 digitalWrite(motorPinL2, LOW);
 }
 
  //Left side sees black
  // we want left motor to go and right to stop
  else if(!all_clears){
    digitalWrite(motorPinL, HIGH);

    digitalWrite(motorPinL2, LOW);

    digitalWrite(motorPinR, LOW);

    digitalWrite(motorPinR2, LOW);

  }
  //Right side sees black
  // we want right motor to go and left to stop
  else if(!all_clear){
    digitalWrite(motorPinR, HIGH);

    digitalWrite(motorPinR2, LOW);

    digitalWrite(motorPinL, LOW);

    digitalWrite(motorPinL2, LOW);
  }




}
