import processing.serial.* ;
Serial buggyPort ;
String incoming; 



void setup()
{
  printArray(Serial.list());
  String portName = Serial.list()[2]; //change to match your port
  buggyPort = new Serial(this, portName, 9600);
  delay(1100);
  buggyPort.write("+++");
  delay(1100);
  buggyPort.write("ATID 3204, CH C, CN");
  delay(1100);
  buggyPort.bufferUntil(10);
}

void draw() {
  
}
void serialEvent(Serial buggy){
incoming = buggy.readString();
println(incoming);
}

void keyPressed() {
  
  if (key == TAB){
    buggyPort.write("s");
  } 
    
  if (key == BACKSPACE){
    buggyPort.write("g");
  }
   
}
