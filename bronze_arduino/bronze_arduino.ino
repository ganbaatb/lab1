const int LEYE = A3;
const int REYE = A4;

const int WheelEncoder = 2;
volatile float count = 0;

const int US_TRIG = 9;
const int US_ECHO = 8;

int motorPinL = 5;
int motorPinR = 6;
int motorPinL2 = 4;
int motorPinR2 = 7;

float distance;
float duration;

int counter = 1;
float revs, circumf, diam, travel;

bool willImove = false;

void Turning(int a, int b);
void Stopping( );
void ir_isr();


  void setup() {
    Serial.begin(9600);
    pinMode (LEYE,INPUT);
    pinMode (REYE,INPUT);
  
    pinMode (US_TRIG, OUTPUT); 
    pinMode (US_ECHO, INPUT);
  
    pinMode(motorPinL, OUTPUT);
    pinMode(motorPinR, OUTPUT);
    pinMode(motorPinL2, OUTPUT);
    pinMode(motorPinR2, OUTPUT);

    attachInterrupt(digitalPinToInterrupt(WheelEncoder), ir_isr, RISING);
    pinMode(WheelEncoder, INPUT);

     delay(1100);
     Serial.print("+++");
     delay(1100);
     Serial.println("ATID 3204, CH C,CN");
     delay(1100);
     while(Serial.read()!=-1){};
  }
  

  void loop() {
    
    void serialEvent();
    if(willImove==false){
       Turning(0, 0);
    }
    
    else{
      bool BlackR = digitalRead(LEYE ); 
      bool BlackL = digitalRead(REYE);

      if (BlackL && BlackR ) {
        Turning(70, 140);
        Stopping( );
        void ir_isr();
      }

      else if(!BlackL && !BlackR){
        Turning(100, 100);
        Stopping( );
        void ir_isr();
      }
    
      else if(!BlackL && BlackR){
        Turning(50, 180);
        Stopping( );  
        void ir_isr();
      }

      else if(BlackL && !BlackR){
        Turning(180, 50);
        Stopping( ); 
        void ir_isr();  
      } 
   }
}

 void serialEvent(){
    char input = Serial.read();
    if(input=='g'){
      willImove = true;
      Serial.print("started");
    }
    if(input=='s'){
      willImove = false;
      Serial.print("stopped");
    }
 }
 
  void Turning(int a, int b){
      analogWrite(motorPinL, a);
      analogWrite(motorPinR, b);
      digitalWrite(motorPinL2, LOW);
      digitalWrite(motorPinR2 ,LOW); 
  }

  void Stopping( ){
      digitalWrite (US_TRIG, LOW);
      delayMicroseconds(2);
  
      digitalWrite( US_TRIG, HIGH); //not same as notes but i think correct
      delayMicroseconds (10);
      digitalWrite (US_TRIG ,LOW);
  
      duration = pulseIn(US_ECHO, HIGH);
      distance = duration/58;
   
      if(distance <= 15){
        analogWrite(motorPinL,  LOW);
        analogWrite(motorPinR, LOW);
        digitalWrite(motorPinL2, LOW);
        digitalWrite(motorPinR2, LOW);
        Serial.print("Obstacle is ");
        Serial.print(distance);
        Serial.print("cm ");
        delay(1000);
    }
  }
  
void ir_isr(){
    count = count+1;
    revs = (count / 5);
    diam = 0.062;
    circumf = PI*diam;
    travel = revs * circumf;  
    if(revs == counter){
        Serial.print("IR counter: ");
        Serial.print(travel);
        Serial.print(" meters");
        counter = counter + 1;
        Serial.println();
        counter = counter + 1;
        }
}















